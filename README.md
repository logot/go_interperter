# Writing An Interpreter In Go

## The order in learning

1. Token
2. Lexer
3. REPL
4. AST & Parser
5. Object & Evaluator
6. Macro System

## How the interpreter works

String --(lexing)-> Token --(parsing)-> AST --(evaluating)-> Output
