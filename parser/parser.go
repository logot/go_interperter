package parser

import (
	"fmt"
	"monkey/ast"
	"monkey/lexer"
	"monkey/token"
	"strconv"
)

// x needs to be able to point to any expressions
// var monkey_binding = `
//   let x = 5;
//   let y = 10;
//   let foobar = add(5, 5);
//   let barfoo = 5 * 5 / 10 + 18 - add(5, 5) + multiply(124);
//   let anotherName = barfoo;

type Parser struct {
	l      *lexer.Lexer
	errors []string

	// act exactly like the two "pointers" like our lexer has:
	// To decide what to do next
	curToken  token.Token
	peekToken token.Token

	// distinguish token position
	prefixParseFns map[token.TokenType]prefixParseFn
	infixParseFns  map[token.TokenType]infixParseFn
}

// defining precedence
const (
	_ int = iota
	LOWEST
	EQUALS      // ==
	LESSGREATER // > or <
	SUM         // +
	PRODUCT     // *
	PREFIX      // -X or !X
	CALL        // myFunction(X)
	INDEX       // myFunction(X)
)

// give the precedence each token
var precedences = map[token.TokenType]int{
	token.EQ:       EQUALS,
	token.NE:       EQUALS,
	token.LT:       LESSGREATER,
	token.GT:       LESSGREATER,
	token.PLUS:     SUM,
	token.MINUS:    SUM,
	token.SLASH:    PRODUCT,
	token.ASTERISK: PRODUCT,
	token.LPAREN:   CALL,
	token.LBRACKET: INDEX,
}

func New(l *lexer.Lexer) *Parser {
	p := &Parser{
		l:      l,
		errors: []string{},
	}

	// Read two tokens, so curToken and peekToken are both set
	p.nextToken()
	p.nextToken()

	// initilize prefixParseFns
	p.prefixParseFns = make(map[token.TokenType]prefixParseFn)
	// add prefix for expressions
	p.registerPrefix(token.IDENT, p.parseIdentifier)
	p.registerPrefix(token.INT, p.parseIntegerLiteral)
	p.registerPrefix(token.STRING, p.parseStringLiteral)
	p.registerPrefix(token.BANG, p.parsePrefixExpression)
	p.registerPrefix(token.MINUS, p.parsePrefixExpression)
	p.registerPrefix(token.TRUE, p.parseBoolean)
	p.registerPrefix(token.FALSE, p.parseBoolean)
	p.registerPrefix(token.LPAREN, p.parseGroupedExpression)
	p.registerPrefix(token.IF, p.parseIfExpression)
	p.registerPrefix(token.FUNCTION, p.parseFunctionLiteral)
	p.registerPrefix(token.LBRACKET, p.parseArrayLiteral)
	p.registerPrefix(token.LBRACE, p.parseHashLiteral)
	p.registerPrefix(token.MACRO, p.parseMacroLiteral)

	// infix
	p.infixParseFns = make(map[token.TokenType]infixParseFn)
	p.registerInfix(token.PLUS, p.parseInfixExpression)
	p.registerInfix(token.MINUS, p.parseInfixExpression)
	p.registerInfix(token.SLASH, p.parseInfixExpression)
	p.registerInfix(token.ASTERISK, p.parseInfixExpression)
	p.registerInfix(token.EQ, p.parseInfixExpression)
	p.registerInfix(token.NE, p.parseInfixExpression)
	p.registerInfix(token.LT, p.parseInfixExpression)
	p.registerInfix(token.GT, p.parseInfixExpression)
	p.registerInfix(token.LPAREN, p.parseCallExpression)
	p.registerInfix(token.LBRACKET, p.parseIndexExpression)

	return p
}

// get parser errors
func (p *Parser) Errors() []string {
	return p.errors
}

// check peek is error - doesn't stop here! (because of more efficient debugging)
func (p *Parser) peekError(t token.TokenType) {
	msg := fmt.Sprintf("expected next token to be %s, got %s instead", t, p.peekToken.Type)
	p.errors = append(p.errors, msg)
}

func (p *Parser) nextToken() {
	p.curToken = p.peekToken
	p.peekToken = p.l.NextToken()
}

func (p *Parser) ParseProgram() *ast.Program {
	program := &ast.Program{}
	program.Statements = []ast.Statement{}

	for p.curToken.Type != token.EOF {
		stmt := p.parseStatement()
		if stmt != nil {
			program.Statements = append(program.Statements, stmt)
		}
		p.nextToken()
	}
	return program
}

func (p *Parser) parseStatement() ast.Statement {
	switch p.curToken.Type {
	case token.LET:
		return p.parseLetStatement()
	case token.RETURN:
		return p.parseReturnStatement()
	default:
		return p.parseExpressionStatement()
	}
}

func (p *Parser) parseLetStatement() *ast.LetStatement {
	// what if : let x = 5;
	// curToken : Token{Type: token.LET,Literal: "let"}
	stmt := &ast.LetStatement{Token: p.curToken}
	// check if peek isn't identifier and advance tokens
	if !p.expectedPeek(token.IDENT) {
		return nil
	} // curToken : Token{Type: token.IDENT,Literal: "x"}
	stmt.Name = &ast.Identifier{Token: p.curToken, Value: p.curToken.Literal}

	// check if peek isn't ASSIGN token and advance tokens
	if !p.expectedPeek(token.ASSIGN) {
		return nil
	} // curToken : Token{Type: token.ASSIGN,Literal: "="}
	p.nextToken() // skip `=`

	// finally we can make a Let statement!
	// check rhs of the assign tokens
	// 5 + 10 * 5 ...
	stmt.Value = p.parseExpression(LOWEST) // parse expression
	if p.peekTokenIs(token.SEMICOLON) {
		p.nextToken()
	}

	return stmt
}

func (p *Parser) parseReturnStatement() *ast.ReturnStatement {
	// curToken : Token{Type: token.RETURN,Literal: "return"}
	stmt := &ast.ReturnStatement{Token: p.curToken}
	// TODO: return stmt if peek is SEMICOLON (return;)
	p.nextToken() // skip `return`
	stmt.ReturnValue = p.parseExpression(LOWEST)

	// check if peek isn't SEMICOLON and advance current token to the semicolon
	// SEMICOLON is optional -> use peekTokenIs(), not expectedPeek()
	if p.peekTokenIs(token.SEMICOLON) {
		p.nextToken() // go to SEMICOLON
	}

	return stmt
}

func (p *Parser) parseBlockStatement() *ast.BlockStatement {
	// defer untrace(trace("parseBlockStatement"))
	block := &ast.BlockStatement{Token: p.curToken}
	block.Statements = []ast.Statement{}

	p.nextToken() // skip LBRACE

	for !p.curTokenIs(token.RBRACE) {
		// to ensure the block is ended with RBRACE
		if p.curTokenIs(token.EOF) {
			msg := fmt.Sprintf("Block is not ended with RBRACE")
			p.errors = append(p.errors, msg)
			return nil
		}

		stmt := p.parseStatement()
		if stmt != nil {
			block.Statements = append(block.Statements, stmt)
		}
		p.nextToken() // move from state
	}

	return block
}

func (p *Parser) parseExpressionStatement() *ast.ExpressionStatement {
	// defer untrace(trace("parseExpressionStatement"))
	stmt := &ast.ExpressionStatement{Token: p.curToken}

	// NOTE: add lowset procedences at the beginning
	stmt.Expression = p.parseExpression(LOWEST)

	if p.peekTokenIs(token.SEMICOLON) {
		p.nextToken()
	}

	return stmt
}

func (p *Parser) noPrefixParseFnError(t token.TokenType) {
	msg := fmt.Sprintf("no prefix parse function for %s found", t)
	p.errors = append(p.errors, msg)
}

func (p *Parser) noInfixParseFnError(t token.TokenType) {
	msg := fmt.Sprintf("no infix parse function for %s found", t)
	p.errors = append(p.errors, msg)
}

// NOTE: parsing expression depends on the caller's knowledge and its context
func (p *Parser) parseExpression(precedence int) ast.Expression {
	// defer untrace(trace("parseExpression"))
	// get prefix parse function of current token's type from parser
	// "Does we have parse function in prefix position?"
	prefix := p.prefixParseFns[p.curToken.Type]

	// if doesn't, return nil
	if prefix == nil {
		p.noPrefixParseFnError(p.curToken.Type)
		return nil
	}

	// run prefix function
	leftExp := prefix()

	// check the rest - this must be infix ( can't be prefix )
	// repeat until encounter `;` or current precedence is greater than peek
	// peekTokenIs is not necessary because peekPrecedence method returns LOWEST constant as the default value if no precedence for current Token but it makes the behaviour more understandable and readable
	// NOTE: precedence : Right Binding Power of curToken (The power of sucked in to the right value of the curToken)
	// p.peekPrecedence : Left Binding Power of peekToken - current token binds to the right value of the infix expression
	for !p.peekTokenIs(token.SEMICOLON) && precedence < p.peekPrecedence() {
		infix := p.infixParseFns[p.peekToken.Type]

		if infix == nil {
			return leftExp
		}

		// advance token
		p.nextToken()

		// advance expression
		leftExp = infix(leftExp)
	}

	return leftExp
}

// this is never advance current token
func (p *Parser) parseIdentifier() ast.Expression {
	return &ast.Identifier{Token: p.curToken, Value: p.curToken.Literal}
}

//	func (p *Parser) parseIntegerLiteral() *ast.IntegerLiteral {
//		return &ast.IntegerLiteral{Token: p.curToken, Value: p.curToken.Literal}
//	}
func (p *Parser) parseIntegerLiteral() ast.Expression {
	// defer untrace(trace("parseIntegerLiteral"))
	lit := &ast.IntegerLiteral{Token: p.curToken}

	value, err := strconv.ParseInt(p.curToken.Literal, 0, 64)
	if err != nil {
		msg := fmt.Sprintf("could not parse %q as integer", p.curToken.Literal)
		p.errors = append(p.errors, msg)
		return nil
	}

	lit.Value = value

	return lit
}

func (p *Parser) parseStringLiteral() ast.Expression {
	return &ast.StringLiteral{Token: p.curToken, Value: p.curToken.Literal}
}

// func (p *Parser) parseBoolean() ast.Expression {
// 	defer untrace(trace("parseBoolean"))
// 	b := &ast.Boolean{Token: p.curToken}
//
// 	value, err := strconv.ParseBool(p.curToken.Literal)
// 	if err != nil {
// 		msg := fmt.Sprintf("could not parse %q as boolean", p.curToken.Literal)
// 		p.errors = append(p.errors, msg)
// 	}
//
// 	b.Value = value
//
// 	return b
// }

func (p *Parser) parseBoolean() ast.Expression {
	// defer untrace(trace("parseBoolean"))
	// NOTE: obtain boolean by comparing the token.TRUE
	return &ast.Boolean{Token: p.curToken, Value: p.curTokenIs(token.TRUE)}
}

func (p *Parser) parsePrefixExpression() ast.Expression {
	// defer untrace(trace("parsePrefixExpression"))
	expression := &ast.PrefixExpression{
		Token:    p.curToken,
		Operator: p.curToken.Literal,
	}

	// advance token
	p.nextToken()

	// NOTE: parse expression recursively
	expression.Right = p.parseExpression(PREFIX)

	return expression
}

func (p *Parser) parseInfixExpression(left ast.Expression) ast.Expression {
	// defer untrace(trace("parseInfixExpression"))
	expression := &ast.InfixExpression{
		Token:    p.curToken,
		Operator: p.curToken.Literal,
		Left:     left,
	}

	// for separating based on precedence
	precedence := p.curPrecedence()
	p.nextToken()
	// parse rest expressions recursively
	// if expression.Operator == "+" {
	// 	expression.Right = p.parseExpression(precedence - 1)
	// } else {
	// 	expression.Right = p.parseExpression(precedence)
	// }
	expression.Right = p.parseExpression(precedence)

	return expression
}

func (p *Parser) parseGroupedExpression() ast.Expression {
	p.nextToken() // skip LPAREN

	// Right Binding Power : Lowest -> Stoped at `)` (p.peekPrecedence() == LOWEST)
	exp := p.parseExpression(LOWEST)

	if !p.expectedPeek(token.RPAREN) {
		return nil
	}

	return exp
}

func (p *Parser) parseIfExpression() ast.Expression {
	// defer untrace(trace("parseIfExpression"))
	expression := &ast.IfExpression{Token: p.curToken}

	if !p.expectedPeek(token.LPAREN) {
		return nil
	}
	p.nextToken() // move to the expression

	expression.Condition = p.parseExpression(LOWEST)

	if !p.expectedPeek(token.RPAREN) { // check & move to RPAREN
		return nil
	}
	if !p.expectedPeek(token.LBRACE) { // check & move to LBRACE
		return nil
	}

	expression.Consequence = p.parseBlockStatement()

	if p.peekTokenIs(token.ELSE) {
		p.nextToken()
		if !p.expectedPeek(token.LBRACE) {
			return nil
		}
		expression.Alternative = p.parseBlockStatement()
	}

	return expression
}

func (p *Parser) parseFunctionLiteral() ast.Expression {
	lit := &ast.FunctionLiteral{Token: p.curToken}

	if !p.expectedPeek(token.LPAREN) {
		return nil
	}

	lit.Parameters = p.parseFunctionParamenters()

	if !p.expectedPeek(token.LBRACE) {
		return nil
	}

	lit.Body = p.parseBlockStatement()

	return lit
}

func (p *Parser) parseFunctionParamenters() []*ast.Identifier {
	identifiers := []*ast.Identifier{}

	p.nextToken() // skip LPARAN

	// if there is no parameters
	if p.curTokenIs(token.RPAREN) {
		return identifiers
	}

	ident := p.parseIdentifier().(*ast.Identifier)
	identifiers = append(identifiers, ident)

	// for !p.curTokenIs(token.RPAREN) {
	// 	if p.peekTokenIs(token.COMMA) {
	// 		p.nextToken()
	// 		p.parseIdentifier()
	// 	}
	// 	p.nextToken()
	// }
	for p.peekTokenIs(token.COMMA) {
		p.nextToken() // go to COMMA
		p.nextToken() // skip COMMA
		ident = p.parseIdentifier().(*ast.Identifier)
		identifiers = append(identifiers, ident)
	}

	// at this time, peekTokenIs NOT COMMA => expect RPAREN
	if !p.expectedPeek(token.RPAREN) {
		return nil
	}

	return identifiers
}

func (p *Parser) parseCallExpression(left ast.Expression) ast.Expression {
	exp := &ast.CallExpression{Token: p.curToken, Function: left}
	exp.Arguments = p.parseExpressionList(token.RPAREN)
	return exp
}

// INFO: input = ["test();", "test(1, 2);", "test(a, b, 8 + c);"]
// func (p *Parser) parseCallArguments() []ast.Expression {
// 	// expression := []ast.Expression{}
// 	args := []ast.Expression{}
//
// 	// If there are no arguments, return directly.
// 	if p.peekTokenIs(token.RPAREN) {
// 		p.nextToken() // go to the RPAREN
// 		return args
// 	}
//
// 	p.nextToken() // skip the LPAREN
//
// 	// INFO: My code -- two of conditions
// 	// for !p.peekTokenIs(token.RPAREN) {
// 	// 	args = append(args, p.parseExpression(LOWEST))
// 	// 	if p.peekTokenIs(token.COMMA) {
// 	// 		p.nextToken() // go to COMMA
// 	// 		p.nextToken() // skip COMMA
// 	// 	}
// 	// }
//
// 	args = append(args, p.parseExpression(LOWEST)) // stoped when the peeks is COMMA or RPAREN
//
// 	// parse until the peeks is not the COMMA
// 	for p.peekTokenIs(token.COMMA) {
// 		p.nextToken() // go to COMMA
// 		p.nextToken() // skip COMMA
// 		args = append(args, p.parseExpression(LOWEST))
// 	}
//
// 	if !p.expectedPeek(token.RPAREN) {
// 		return nil
// 	}
//
// 	return args
// }

// input := "[1, 2 * 2, 3 + 3]"
func (p *Parser) parseArrayLiteral() ast.Expression {
	array := &ast.ArrayLiteral{Token: p.curToken}

	array.Elements = p.parseExpressionList(token.RBRACKET)

	return array
}

func (p *Parser) parseExpressionList(end token.TokenType) []ast.Expression {
	list := []ast.Expression{}

	if p.peekTokenIs(end) {
		p.nextToken()
		return list
	}

	p.nextToken() // skip the start
	list = append(list, p.parseExpression(LOWEST))

	for p.peekTokenIs(token.COMMA) {
		p.nextToken() // go to the `,`
		p.nextToken() // skip the `,`
		list = append(list, p.parseExpression(LOWEST))
	}

	if !p.expectedPeek(end) {
		return nil
	}

	return list
}

func (p *Parser) parseIndexExpression(left ast.Expression) ast.Expression {
	indexExp := &ast.IndexExpression{Token: p.curToken, Left: left}
	if p.peekTokenIs(token.RBRACKET) {
		return nil
	}

	p.nextToken() // skip the `[`
	indexExp.Index = p.parseExpression(LOWEST)

	if !p.expectedPeek(token.RBRACKET) {
		return nil
	}

	return indexExp
}

func (p *Parser) parseHashLiteral() ast.Expression {
	hash := &ast.HashLiteral{Token: p.curToken}
	hash.Pairs = make(map[ast.Expression]ast.Expression)

	for !p.peekTokenIs(token.RBRACE) {
		p.nextToken()
		key := p.parseExpression(LOWEST)

		if !p.expectedPeek(token.COLON) {
			return nil
		}

		p.nextToken() // skipping the COLON
		value := p.parseExpression(LOWEST)

		hash.Pairs[key] = value

		// not separated by COMMA
		if !p.peekTokenIs(token.RBRACE) && !p.expectedPeek(token.COMMA) {
			return nil
		}
	}

	if !p.expectedPeek(token.RBRACE) {
		return nil
	}

	return hash
}

func (p *Parser) parseMacroLiteral() ast.Expression {
	macro := &ast.MacroLiteral{Token: p.curToken}

	// go to LPAREN if it exists
	if !p.expectedPeek(token.LPAREN) {
		return nil
	}
	macro.Parameters = p.parseFunctionParamenters()

	if !p.expectedPeek(token.LBRACE) {
		return nil
	}

	macro.Body = p.parseBlockStatement()

	return macro
}

// Helper functions

// check current token type
func (p *Parser) curTokenIs(t token.TokenType) bool {
	return p.curToken.Type == t
}

// check peek token type
func (p *Parser) peekTokenIs(t token.TokenType) bool {
	return p.peekToken.Type == t
}

// Advance tokens while making assertion
func (p *Parser) expectedPeek(t token.TokenType) bool {
	if p.peekTokenIs(t) {
		p.nextToken()
		return true
	} else {
		p.peekError(t)
		return false
	}
}

// Add Pratt Parser's features
type (
	prefixParseFn func() ast.Expression
	infixParseFn  func(ast.Expression) ast.Expression
)

// associate function to prifix functions
func (p *Parser) registerPrefix(tokenType token.TokenType, fn prefixParseFn) {
	p.prefixParseFns[tokenType] = fn
}

func (p *Parser) registerInfix(tokenType token.TokenType, fn infixParseFn) {
	p.infixParseFns[tokenType] = fn
}

// return peek precedence from table
func (p *Parser) peekPrecedence() int {
	if p, ok := precedences[p.peekToken.Type]; ok {
		return p
	}

	return LOWEST
}

// return current precedence from table
func (p *Parser) curPrecedence() int {
	if p, ok := precedences[p.curToken.Type]; ok {
		return p
	}

	return LOWEST
}
